package org.matt1.exercisecalculator;

import java.text.NumberFormat;

public class Calculator {

	public static Calculator instance = new Calculator();
	private static double METERS_IN_MILE = 1609.344;
	private static double MILES_IN_METER = 0.000621371192;
	private static double METERS_IN_1KPH = 16.6666667;
	private static double KG_IN_POUND = 0.453592;
	private static double CM_IN_INCH = 2.54;
	static NumberFormat formater = NumberFormat.getNumberInstance();
	static {
		formater.setMaximumFractionDigits(2);
	}
	
	
	public static Calculator getInstance() {
		return instance;
	}

	public NumberFormat getFormat() {
		return formater;
	}
	
	/**
	 * Get km in miles
	 * 
	 * @param km
	 * @return
	 */
	public double KmToMiles(double km) {
		return metersToMiles(Math.abs(km)) * 1000;
	}
	
	/**
	 * Get miles in meters 
	 * 
	 * @param meters
	 * @return
	 */
	public double metersToMiles(double meters) {
		return Math.abs(meters) * MILES_IN_METER;
	}
	
	/**
	 * Get miles as KM
	 * 
	 * @param miles
	 * @return
	 */
	public double milesToKm(double miles) {
		return milesToMeters(Math.abs(miles))/1000;
	}
	
	/**
	 * Get inches as CM
	 * 
	 * @param inches
	 * @return
	 */
	public double inchesToCm(double inches) {
	  return inches * CM_IN_INCH;
	}
	
	/**
	 * Get miles as meters
	 * 
	 * @param miles
	 * @return
	 */
	public double milesToMeters(double miles) {
		return Math.abs(miles) * METERS_IN_MILE;
	}
	
	/**
	 * Get speed as meters per min from miles, e.g. 1 mile in 10 mins = 1609m per min
	 * 
	 * @param miles
	 * @param time in minutes
	 * @return
	 */
	public double getSpeedFromMiles(double miles, double time) {
		return getSpeedFromKM(milesToKm(miles), time);
	}
	
	/**
	 * Get speed as meters per min from KM
	 * 
	 * @param km
	 * @param time
	 * @return
	 */
	public double getSpeedFromKM(double km, double time) {
		double result = 0d;
		if (km <= 0 || time <= 0) {
			return result;
		}
		
		double speed = km/(time/60.0d);
		result = speed * METERS_IN_1KPH;
		
		return result;
	}
	
	/**
	 * Get calories burnt from VO2
	 * 
	 * @param vo2
	 * @return
	 */
	public double getCalorieFromVO2(double vo2) {
		return Math.abs(vo2) * 5;
	}
	
	/**
	 * get Calories burnt walking
	 * 
	 * @param distance in KM
	 * @param time
	 * @param mass
	 * @return
	 */
	public double getCaloriesWalkingKm(double distance, double time, double mass) {
		double result = 0d;
		if (distance <= 0 || time <=0 || mass <= 0) {
			return result;
		}
		
		double speed = getSpeedFromKM(distance, time);
		double vo2 = (0.1 * speed) + (1.8 * speed * 0) + 3.5;
		double o2LitresMin = (vo2 * mass) / 1000;
		result = getCalorieFromVO2(o2LitresMin * time);
		
		
		return result;
	}

	/**
	 * get Calories burnt Running
	 * 
	 * @param distance in KM
	 * @param time
	 * @param mass
	 * @return
	 */
	public double getCaloriesRunningKm(double distance, double time, double mass, double grade) {
		double result = 0d;
		if (distance <= 0 || time <=0 || mass <= 0) {
			return result;
		}
		
		double speed = getSpeedFromKM(distance, time);
		double vo2 = (0.2 * speed) + (0.9 * speed * grade) + 3.5;
		double o2LitresMin = (vo2 * mass) / 1000;
		result = getCalorieFromVO2(o2LitresMin * time);
		
		
		return result;
	}
	
	/**
	 * Get calories burnt running miles
	 * 
	 * @param distance in miles
	 * @param time
	 * @param mass
	 * @return
	 */
	public double getCaloriesRunningMiles(double distance, double time, double mass, double grade) {
		return getCaloriesRunningKm(milesToKm(distance),time,mass,grade);
	}
	
	/**
	 * Get calories burnt walking miles
	 * 
	 * @param distance in miles
	 * @param time
	 * @param mass
	 * @return
	 */
	public double getCaloriesWalkingMiles(double distance, double time, double mass) {
		return getCaloriesWalkingKm(milesToKm(distance),time,mass);
	}
	
	/**
	 * Get calories burnt stepping (assumes 100 steps/min)
	 * 
	 * @param steps
	 * @param stepHeight in metres
	 * @return
	 */
	public double getCaloriesStepping(double steps, double stepHeight, double stepRate, double mass) {
		double result = 0d;
		
		if (steps <= 0 || stepHeight <=0) {
			return result;
		}
		
		double stepsPerMin = stepRate;
		double vo2 = (0.2 * stepsPerMin) + (1.33 * 1.8 * stepHeight * stepsPerMin) + 3.5;
		double o2LitresMin = (vo2 * mass) / 1000;
		result = getCalorieFromVO2(o2LitresMin * (steps/stepsPerMin));

		return result;
	}
	
	/**
	 * Get calories stepping (assumes 15cm step height and 70steps/min)
	 * @param steps
	 * @param mass
	 * @return
	 */
	public double getCaloriesStepping(double steps, double mass) {
	  double result = 0d;
	   if (steps <=0 || mass <= 0) {
	      return result;
	    }
	   
	   return getCaloriesStepping(steps, 0.15, 70, mass);
	}
	
	/**
	 * Gets calories burnt walking at a given pace
	 * 
	 * @param pace kmph
	 * @param time in minutes
	 * @return
	 */
	public double getCaloriesWalkingPace(double pace, double time, double mass) {
		double result = 0d;
		
		if (pace <=0 || time <= 0) {
			return result;
		}
		
		double distance = (time/60.0d) * pace;
		result = getCaloriesWalkingKm(distance, time, mass);
		return result;
	}
	
	public double getKgsInPound(double weight) {
		double result = 0d;
		
		if (weight <=0) {
			return result;
		}
		
		return weight * KG_IN_POUND;
	}
}
