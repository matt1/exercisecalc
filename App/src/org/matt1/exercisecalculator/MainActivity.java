package org.matt1.exercisecalculator;

import java.util.HashMap;
import java.util.Map;

import org.matt1.exercisecalculator.framents.ParentFragment;
import org.matt1.exercisecalculator.framents.Running;
import org.matt1.exercisecalculator.framents.Stepping;
import org.matt1.exercisecalculator.framents.Walking;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SpinnerAdapter;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends Activity {

  /** Keep track of if we are in simple view or not */
  protected boolean isAdvancedView = true;

  /** Keep track of current view so we can toggle between simple-advanced easily */
  protected int currentViewIndex = 0;

  /** SpinnerAdapter for the drop down navigation */
  SpinnerAdapter mDropDownAdapter;

  /** Store the menu for dynamic changes */
  private Menu actionBarMenu;
  
  /** Enum of the tabs */
  private enum TabType {
	  WALK("walkFrament"),
	  RUN("runFragment"),
	  STEP("stepFragment");
	  
	  private String tag;
	  TabType(String tag) {
		  this.tag = tag;
	  }
	  
	  public String getTag() {
		  return this.tag;
	  }
  }
  
  /** Contains mappings of tab to fragment.  Needed to update state in child fragments */
  Map<TabType, ParentFragment> fragmentMap = new HashMap<TabType, ParentFragment>(3);
  
  /** AdView for advertisements */
  private AdView adView;
  
  /**
   * Get if we're in the advanced view or not
   * @return
   */
  public boolean getIsAdvanced() {
    return isAdvancedView;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    
    // Settings manager
    SettingsManager.setActivity(this);
    
    // Look up the AdView as a resource and load a request.
    AdView adView = (AdView) this.findViewById(R.id.adView);
    AdRequest adRequest = new AdRequest.Builder()
      .addTestDevice("5B3A0F57FDF996D95F081801B90E5590").build();
    adView.loadAd(adRequest);
    
    // Action bar
    getActionBar().setDisplayShowTitleEnabled(false);
    getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

    Tab tabA = getActionBar().newTab();
    tabA.setText("Walk");
    tabA.setTabListener(
    		new TabListener<Walking>(this, TabType.WALK.getTag(), Walking.class, fragmentMap, 
    		    TabType.WALK, R.id.mainLayoutTabHolder));
    getActionBar().addTab(tabA);
    
    Tab tabB = getActionBar().newTab();
    tabB.setText("Run");
    tabB.setTabListener(
    		new TabListener<Running>(this, TabType.RUN.getTag(), Running.class, fragmentMap, 
    		    TabType.RUN, R.id.mainLayoutTabHolder));
    getActionBar().addTab(tabB);
    
    Tab tabC = getActionBar().newTab();
    tabC.setText("Step");
    tabC.setTabListener(
    		new TabListener<Stepping>(this, TabType.STEP.getTag(), Stepping.class, fragmentMap, 
    		    TabType.STEP, R.id.mainLayoutTabHolder));
    getActionBar().addTab(tabC);

    if (savedInstanceState != null) {
      int savedIndex = savedInstanceState.getInt("SAVED_INDEX");
      getActionBar().setSelectedNavigationItem(savedIndex);
    }

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    this.actionBarMenu = menu;
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle presses on the action bar items
    switch (item.getItemId()) {
      case R.id.actionSimple:
        isAdvancedView = false;
        break;
      case R.id.actionAdvanced:
        isAdvancedView = true;
        break;
    }
    updateModeView();
    return true;
  }

  /**
   * Updates the fragments to show the appropriate view
   */
  private void updateModeView() {

    int[] simpleIds = { R.id.walkingSimple, R.id.runningSimple, R.id.steppingSimple };
    int[] advancedIds = { R.id.walkingAdvanced, R.id.runningAdvanced, R.id.steppingAdvanced };

    if (!isAdvancedView) {
      for (int simpleId : simpleIds) {
        if (findViewById(simpleId) != null) {
          findViewById(simpleId).setVisibility(View.VISIBLE);
        }
      }
      for (int advancedId : advancedIds) {
        if (findViewById(advancedId) != null) {
          findViewById(advancedId).setVisibility(View.GONE);
        }
      }

    } else {
      for (int simpleId : simpleIds) {
        if (findViewById(simpleId) != null) {
          findViewById(simpleId).setVisibility(View.GONE);
        }
      }
      for (int advancedId : advancedIds) {
        if (findViewById(advancedId) != null) {
          findViewById(advancedId).setVisibility(View.VISIBLE);
        }
      }

    }
    
    // Update state handlers in fragments
    for (ParentFragment tab : fragmentMap.values()) {
  	  tab.setIsAdvanced(isAdvancedView);
  	  tab.calculate();
    }
    
    // Update menus too
    if (this.actionBarMenu != null) {
      this.actionBarMenu.findItem(R.id.actionSimple).setVisible(isAdvancedView);
      this.actionBarMenu.findItem(R.id.actionAdvanced).setVisible(!isAdvancedView);
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putInt("SAVED_INDEX", getActionBar().getSelectedNavigationIndex());
  }

  public static class TabListener<T extends Fragment> implements ActionBar.TabListener {

    private final MainActivity myActivity;
    private final String myTag;
    private final Class<T> myClass;
    private Map<TabType, ParentFragment> fragmentMap;
    private TabType type;
    private int view;

    public TabListener(MainActivity activity, String tag, Class<T> cls, 
    		Map<TabType, ParentFragment> fragmentMap, TabType type, int viewId) {
      myActivity = activity;
      myTag = tag;
      myClass = cls;
      this.fragmentMap = fragmentMap;
      this.type = type;
      this.view = viewId;
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
      Fragment myFragment = myActivity.getFragmentManager().findFragmentByTag(myTag);
      if (myFragment == null) {
        myFragment = Fragment.instantiate(myActivity, myClass.getName());
        ft.add(this.view , myFragment, myTag);
        ((ParentFragment) myFragment).setActivity(myActivity);
        fragmentMap.put(type, (ParentFragment) myFragment);
        myActivity.updateModeView();
      } else {
        ft.attach(myFragment);
      }

    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
      Fragment myFragment = myActivity.getFragmentManager().findFragmentByTag(myTag);
      if (myFragment != null) {
        ft.detach(myFragment);
      }
    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {}
  }
  
  @Override
  public void onPause() {
    if (adView != null) {
      adView.resume();
    }
    super.onPause();
  }

  @Override
  public void onResume() {
    super.onResume();
    if (adView != null) {
      adView.resume();
    }
  }

  @Override
  public void onDestroy() {
    if (adView != null) {
      adView.resume();
    }
    super.onDestroy();
  }

  /**
   * Handle scree rotation state-loss due to activity restart
   */
  @Override
  public void onConfigurationChanged(Configuration config) {
	  updateModeView();
	  super.onConfigurationChanged(config);
  }
  
}

