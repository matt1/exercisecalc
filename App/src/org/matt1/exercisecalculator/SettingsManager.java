package org.matt1.exercisecalculator;

import android.app.Activity;
import android.content.SharedPreferences;

/**
 * Handles static settings to avoid complicated nonsense
 * 
 * @author Matt
 * 
 */
public class SettingsManager {

	private static SettingsManager instance = new SettingsManager();
	private static Activity parentActivity;
	private static String PREFS_FILE = "exercisecalulator_prefs";
	private static String PREFS_WEIGHT_VALUE = "weightValue";
	private static String PREFS_WEIGHT_UNIT = "weightUnit";

	public static SettingsManager getInstance() {
		return instance;
	}
	
	public static void setActivity (Activity pActivty) {
		parentActivity = pActivty;
	}

	/**
	 * Persist weight
	 * 
	 * @param value
	 * @param units
	 */
	public void persistWeight(double value, int units) {
		SharedPreferences settings = parentActivity.getSharedPreferences(PREFS_FILE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putFloat(PREFS_WEIGHT_VALUE, (float) value);
		editor.commit();
		settings.edit();
		editor.putInt(PREFS_WEIGHT_UNIT, units);
		editor.commit();
	}

	public double getPersistedWeightValue() {
		SharedPreferences settings = parentActivity.getSharedPreferences(PREFS_FILE, 0);
		float val = settings.getFloat(PREFS_WEIGHT_VALUE, 0);
		return val ;
	}

	public int getPersistedWeightUnit() {
		SharedPreferences settings = parentActivity.getSharedPreferences(PREFS_FILE, 0);
		return settings.getInt(PREFS_WEIGHT_UNIT, 0);
	}

}
