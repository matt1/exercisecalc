package org.matt1.exercisecalculator.framents;

import org.matt1.exercisecalculator.R;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class Running extends ParentFragment {

  private EditText simpleDistance;
  private double simpleDistanceValue;
  private EditText simpleTime;
  private double simpleTimeValue;
  private Spinner simpleDistanceUnit;
  
  private TextView results;
    
  private EditText advancedDistance;
  private double advancedDistanceValue;
  private EditText advancedTime;
  private double advancedTimeValue;
  private EditText advancedGrade;
  private double advancedGradeValue;  
  
  private Spinner advancedDistanceUnit;
  

  
	@Override
    public View onCreateView(LayoutInflater infl, ViewGroup container,Bundle savedInstanceState) {
        rootView = (ViewGroup) infl.inflate(R.layout.running, container, false);
        setupWeight(rootView);
        
        // Results
        results = (TextView) rootView.findViewById(R.id.runningResultsValue);
        
        TextWatcher simpleChanger = new TextWatcher() {
          @Override
          public void onTextChanged(CharSequence s, int start, int before, int count) {
            simpleDistanceValue = toDouble(simpleDistance.getText().toString());
            simpleTimeValue = toDouble(simpleTime.getText().toString());
            advancedDistanceValue = toDouble(advancedDistance.getText().toString());
            advancedGradeValue = toDouble(advancedGrade.getText().toString());
            if (advancedGradeValue > 1) {
            	advancedDistanceValue = 1;
            	advancedGrade.setText("1");
            }
            advancedTimeValue = toDouble(advancedTime.getText().toString());
            calculate();
          }
          @Override
          public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
          @Override
          public void afterTextChanged(Editable s) {}
        };
        
        // Simple Controls
        simpleDistance = (EditText) rootView.findViewById(R.id.runningSimpleDistance);
        simpleTime = (EditText) rootView.findViewById(R.id.runningSimpleTime);
        simpleDistanceUnit = (Spinner) rootView.findViewById(R.id.runningSimpleDistanceUnits);
        simpleTime.addTextChangedListener(simpleChanger);
        simpleDistance.addTextChangedListener(simpleChanger);
        
        // simple distance unit spinner
        simpleDistanceUnit.setOnItemSelectedListener(new OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            distanceUnit = DistanceUnit.values()[(int) simpleDistanceUnit.getSelectedItemId()];
            calculate();
          }
          @Override
          public void onNothingSelected(AdapterView<?> arg0) {}
        });
        
        //Advanced Controls
        advancedDistance = (EditText) rootView.findViewById(R.id.runningAdvancedDistance);
        advancedTime = (EditText) rootView.findViewById(R.id.runningAdvancedTime);
        advancedGrade = (EditText) rootView.findViewById(R.id.runningAdvancedGrade);
        advancedDistanceUnit = (Spinner) rootView.findViewById(R.id.runningAdvancedDistanceUnits);
        advancedDistance.addTextChangedListener(simpleChanger);
        advancedTime.addTextChangedListener(simpleChanger);
        advancedGrade.addTextChangedListener(simpleChanger);
        
        // advanced distance unit spinner
        advancedDistanceUnit.setOnItemSelectedListener(new OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            distanceUnit = DistanceUnit.values()[(int) advancedDistanceUnit.getSelectedItemId()];
            calculate();
          }
          @Override
          public void onNothingSelected(AdapterView<?> arg0) {}
        });

        viewToggle(rootView, R.id.runningSimple, R.id.runningAdvanced);
        return rootView;
	}
	
	protected void calculateSimple() {
		if (isAdvanced) {
			return;
  		}
  
		double distance = simpleDistanceValue;
		if (distanceUnit == DistanceUnit.MILES) {
		  distance = calculator.milesToKm(distance);
		}
		double result = calculator.getCaloriesRunningKm(distance, simpleTimeValue, getEnteredWeight(), 0);
		if (results != null) {
			if (result > 0) {
			  results.setText(calculator.getFormat().format(result)); 
			} else {
			  results.setText("0.0");
			}
		}

	}
	
	protected void calculateAdvanced() {
	  if (!isAdvanced) {
	    return;
	  }
	  
	    double distance = advancedDistanceValue;
	    if (distanceUnit == DistanceUnit.MILES) {
	      distance = calculator.milesToKm(distance);
	    }
	    double result = calculator.getCaloriesRunningKm(distance, advancedTimeValue, getEnteredWeight(), 
	        advancedGradeValue);
	    
	    if (results != null) {
		    if (result > 0) {
		      results.setText(calculator.getFormat().format(result)); 
		    } else {
		      results.setText("0.0");
		    }
	    }
	  
  }
	
	
}

