package org.matt1.exercisecalculator.framents;

import org.matt1.exercisecalculator.R;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class Walking extends ParentFragment {

	private EditText simpleTime;
	private Spinner simpleIntensity;
	private double simpleIntensityValue = 2d;
	private double simpleTimeValue = 0d;
	
	private TextView results;
	
	private Spinner advancedDistanceSpinner;
	private EditText advancedTime;
	private EditText advancedDistance;
	private double advancedTimeValue = 0d;
	private double advancedDistanceValue = 0d;

	enum speeds {
		TWO(2),
		TWOHALF(2.5),
		THREE(3),
		THREEHALF(3.5),
		FOUR(4),
		FOURHALF(4.5),
		FIVE(5);
		
		private double mph;
		speeds(double speed) {
			this.mph = speed;
		}
		public double getSpeed() {
			return mph;
		}
	}
	@Override
	public void onResume() {
		setupWeight(rootView);
		super.onResume();
	}
	@Override
    public View onCreateView(LayoutInflater infl, ViewGroup container, Bundle savedInstanceState) {
        rootView = (ViewGroup) infl.inflate(R.layout.walking, container, false);
        viewToggle(rootView, R.id.walkingSimple, R.id.walkingAdvanced);
        setupWeight(rootView);
        
        // Results
        results = (TextView) rootView.findViewById(R.id.walkingResultsValue);
        
        TextWatcher simpleChanger = new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				simpleTimeValue = toDouble(simpleTime.getText().toString());
				advancedTimeValue = toDouble(advancedTime.getText().toString());
				advancedDistanceValue = toDouble(advancedDistance.getText().toString());
				calculate();
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		};
       
        
        // Simple Controls
        simpleTime = (EditText) rootView.findViewById(R.id.walkingSimpleTime);
        simpleIntensity = (Spinner) rootView.findViewById(R.id.walkingSimpleIntensity);
        simpleTime.addTextChangedListener(simpleChanger);
        
        //Advanced Controls
        advancedTime = (EditText) rootView.findViewById(R.id.walkingAdvancedTime);
        advancedDistance = (EditText) rootView.findViewById(R.id.walkingAdvancedDistance);
        advancedDistanceSpinner = (Spinner) rootView.findViewById(R.id.walkingAdvancedDistanceUnits);
        advancedDistance.addTextChangedListener(simpleChanger);
        advancedTime.addTextChangedListener(simpleChanger);
        
        // intensity spinner
        simpleIntensity.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				simpleIntensityValue = 
						speeds.values()[(int) simpleIntensity.getSelectedItemId()].getSpeed();
			
				calculate();
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {}
		});
        
        advancedDistanceSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				distanceUnit = 
						DistanceUnit.values()[(int) advancedDistanceSpinner.getSelectedItemId()];
				calculate();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {}
		});
        
        return rootView;
	}
	
	@Override
	protected void calculateSimple() {
		if (isAdvanced) {
		  return;
		}
		double paceInKm = calculator.milesToKm(simpleIntensityValue);
		double result = calculator.getCaloriesWalkingPace(paceInKm, simpleTimeValue, getEnteredWeight());
		if (results != null) {
			if (result > 0) {
				results.setText(calculator.getFormat().format(result));	
			} else {
				results.setText("0.0");
			}
		}

	}

	@Override
	protected void calculateAdvanced() {
		if (!isAdvanced) {
		  return;
		}
		
		double distance = advancedDistanceValue;
		if (distanceUnit == DistanceUnit.MILES) {
			distance = calculator.milesToKm(distance);
		}
		
		double result = calculator.getCaloriesWalkingKm(distance, advancedTimeValue, getEnteredWeight());
		
		if (results != null) {
			if (result > 0) {
				results.setText(calculator.getFormat().format(result));	
			} else {
				results.setText("0.0");
			}
		}

	}

}
