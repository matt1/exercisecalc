package org.matt1.exercisecalculator.framents;

import org.matt1.exercisecalculator.Calculator;
import org.matt1.exercisecalculator.R;
import org.matt1.exercisecalculator.SettingsManager;

import android.app.Activity;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;

public class ParentFragment extends Fragment {
 
	/** Units of weight */
	protected enum WeightUnit {
		POUNDS(0),
		KG(1);

		private int index;

		
		private WeightUnit(int index) {
			this.index = index;
		}
		
		public int getIndex() {
			return index;
		}

	}
	
	/** Units of distance */
	protected enum DistanceUnit {
		MILES(0),
		KM(1);

		private int index;
		
		private DistanceUnit(int index) {
			this.index = index;
		}
		
		public int getIndex() {
			return index;
		}
		
	}
	
  /** Units of Height */
  protected enum HeightUnit {
    INCHES(0),
    CM(1);

    private int index;
    
    private HeightUnit(int index) {
      this.index = index;
    }
    
    public int getIndex() {
      return index;
    }
    
  }

  protected Activity parentActivity;
  protected ViewGroup rootView;
	protected EditText weightText;
	protected Spinner weightUnitSpinner;
	protected WeightUnit weightUnit = WeightUnit.POUNDS;
	protected DistanceUnit distanceUnit = DistanceUnit.MILES;
	protected HeightUnit heightUnit = HeightUnit.INCHES;
	protected Calculator calculator = Calculator.getInstance();
	protected Boolean isAdvanced = false;
	
	public void setIsAdvanced(boolean isAdvanced) {
	 this.isAdvanced = isAdvanced;
	}
	
	/**
	 * Setup the toggle for the simple & advanced view
	 * @param simpleView R.id of the simple view
	 * @param advancedView R.id of the advanced view
	 */
	protected void viewToggle(View rootView, int simpleView, int advancedView) {
	  
		View advanced = rootView.findViewById(advancedView);
	    View simple = rootView.findViewById(simpleView);
	    
	    if (this.isAdvanced) {
	      advanced.setVisibility(View.VISIBLE);
	      simple.setVisibility(View.GONE);
	    } else {
	      advanced.setVisibility(View.GONE);
	      simple.setVisibility(View.VISIBLE);
	    }

	}
	

	protected boolean skipWeightUnit = true;
	protected boolean skipWeightValue = true;
	
	@Override
	public void onResume() {
		super.onResume();
		setupWeight(rootView);
		skipWeightUnit = false;
		skipWeightValue = false;
	}
	
	@Override
	public void onPause() {
		super.onPause();
		 skipWeightUnit = true;
		 skipWeightValue = true;
	}
	
	/**
	 * Sets up the handlers etc for weights
	 */
	protected void setupWeight(ViewGroup rootView) {
		
		weightUnitSpinner = (Spinner) rootView.findViewById(R.id.weightUnit);
		weightUnitSpinner.setSelection(SettingsManager.getInstance().getPersistedWeightUnit());
		weightUnitSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,	int arg2, long arg3) {
				weightUnit = WeightUnit.values()[(int) weightUnitSpinner.getSelectedItemId()];
				if (!skipWeightUnit) {
					SettingsManager.getInstance().persistWeight(
						toDouble(weightText.getText().toString()), weightUnit.index);
				}
				calculate();
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {}
		});
	
		weightText = (EditText) rootView.findViewById(R.id.weight);
		String w = calculator.getFormat().format(
				SettingsManager.getInstance().getPersistedWeightValue());
		weightText.setText(w);
		weightText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (!skipWeightValue) {
					SettingsManager.getInstance().persistWeight(
						toDouble(weightText.getText().toString()), weightUnit.index);
				}
				calculate();
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});
	        
	}
	
	/**
	 * Calculate all changes
	 */
	public void calculate() {
		calculateSimple();
		calculateAdvanced();
	}
	protected void calculateSimple(){}
	protected void calculateAdvanced(){};
	
	/**
	 * Get the current entered weight.  This is not getting the persisted weight.
	 */
	protected double getEnteredWeight() {
		double result = 0;
		try {
			result = Double.parseDouble(weightText.getText().toString());
			if (weightUnit == WeightUnit.POUNDS) {
				result = calculator.getKgsInPound(result);
			}
		} catch (Exception nfe) {
			// nop
		}
		return result;
	}
	
	protected double toDouble(String string) {
		double result = 0.0d;
		if (!string.isEmpty() && !string.equals(".") && !string.equals(".0")) {
			result = Double.parseDouble(string);
		}
		return result;
	}
	 
	/**
	 * Set the parent activity
	 * 
	 * @param parent
	 */
	public void setActivity(Activity parent) {
	  this.parentActivity = parent;
	}
	

}