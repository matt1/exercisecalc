package org.matt1.exercisecalculator.framents;

import org.matt1.exercisecalculator.R;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class Stepping extends ParentFragment {
	
	private EditText simpleSteps;
	private int simpleStepsValue;
	private TextView results;
	private EditText advancedSteps;
	private int advancedStepsValue;
	private EditText advancedStepHeight;
	private int advancedStepHeightValue;
	private EditText advancedStepRate;
	private int advancedStepRateValue;
	private Spinner advancedStepHeightUnit;
	
	@Override
    public View onCreateView(LayoutInflater infl, ViewGroup container, Bundle savedInstanceState) {
        rootView = (ViewGroup) infl.inflate(R.layout.stepping, container, false);
        setupWeight(rootView);
        
        // Results
        results = (TextView) rootView.findViewById(R.id.steppingResultsValue);
        
        TextWatcher simpleChanger = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
              simpleStepsValue = (int) toDouble(simpleSteps.getText().toString());
              advancedStepsValue = (int) toDouble(advancedSteps.getText().toString());
              advancedStepHeightValue = (int) toDouble(advancedStepHeight.getText().toString());
              advancedStepRateValue = (int) toDouble(advancedStepRate.getText().toString());
              calculate();
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
          };
        
        // Simple Controls
        simpleSteps = (EditText) rootView.findViewById(R.id.steppingSimpleSteps);
        simpleSteps.addTextChangedListener(simpleChanger);
        
        //Advanced Controls
        advancedSteps = (EditText) rootView.findViewById(R.id.stepsAdvanced);
        advancedStepHeight = (EditText) rootView.findViewById(R.id.stepsAdvancedStepHeight);
        advancedStepRate = (EditText) rootView.findViewById(R.id.stepsAdvancedStepRate);
        advancedStepHeightUnit = (Spinner) rootView.findViewById(R.id.steppingAdvancedHeightUnits);

        advancedSteps.addTextChangedListener(simpleChanger);
        advancedStepHeight.addTextChangedListener(simpleChanger);
        advancedStepRate.addTextChangedListener(simpleChanger);
        
        // intensity spinner
        advancedStepHeightUnit.setOnItemSelectedListener(new OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            heightUnit = 
                HeightUnit.values()[(int) advancedStepHeightUnit.getSelectedItemId()];
            calculate();
          }
          @Override
          public void onNothingSelected(AdapterView<?> arg0) {}
        });
        

        viewToggle(rootView, R.id.steppingSimple, R.id.steppingAdvanced);
        return rootView;
	}
	
	@Override
  protected void calculateSimple() {
    if (isAdvanced) {
      return;
    }
    
    double result = calculator.getCaloriesStepping(simpleStepsValue, getEnteredWeight());
    
    if (results != null) {
	    if (result > 0) {
	      results.setText(calculator.getFormat().format(result)); 
	    } else {
	      results.setText("0.0");
	    }
    }

  }

  @Override
  protected void calculateAdvanced() {
    if (!isAdvanced) {
      return;
    }
    
    double height = advancedStepHeightValue / 100.0d;
    if (heightUnit == HeightUnit.INCHES) {
      height = calculator.inchesToCm(advancedStepHeightValue) / 100.0d;
    }

    double result = calculator.getCaloriesStepping(advancedStepsValue, height, 
    		advancedStepRateValue, getEnteredWeight());
    
    if (results != null) {
	    if (result > 0) {
	      results.setText(calculator.getFormat().format(result)); 
	    } else {
	      results.setText("0.0");
	    }
    }
  }	
}

